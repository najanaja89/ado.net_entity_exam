﻿using ado.net_entity_exam.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_entity_exam.DataContext
{
    public class DataInitializer : DropCreateDatabaseIfModelChanges<MagazineContext>
    {
        protected override void Seed(MagazineContext context)
        {
            Magazine magazine = new Magazine();

            User user = new User
            {
                Login = "najanaja",
                Password = "user",
                FirstName = "ruslan",
                LastName = "tyo",
                Address = "Diagon Alley, house 4",
            };

            User admin = new User
            {
                Login = "admin",
                Password = "root",
                FirstName = "",
                LastName = "",
                Address = "",
               isAdmin=true
              
            };

           

            Сourier сourier = new Сourier
            {
                Login ="slave1",
                FirstName="vasya",
                LastName="pupkin"
            };

            Orders order = new Orders
            {
                UserId = user.Id,
                CourierId = сourier.Id
               
            };

            context.Users.Add(admin);
            context.Users.Add(user);
            context.Magazines.Add(magazine);
            context.Сouriers.Add(сourier);
            context.Orders.Add(order);
            context.SaveChanges();
        }

    }
}
