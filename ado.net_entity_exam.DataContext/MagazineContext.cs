﻿namespace ado.net_entity_exam.DataContext
{
    using ado.net_entity_exam.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class MagazineContext : DbContext
    {
        // Контекст настроен для использования строки подключения "MagazineContext" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "ado.net_entity_exam.DataContext.MagazineContext" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "MagazineContext" 
        // в файле конфигурации приложения.
        public MagazineContext()
            : base("name=MagazineContext")
        {
            Database.SetInitializer(new DataInitializer());
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Сourier> Сouriers { get; set; }
        public DbSet<Magazine> Magazines { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
        }


        // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
        // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}