﻿using ado.net_entity_exam.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_entity_exam.Models
{
    public class Magazine:Entity
    {
        public string Name { get; set; } = "Nomad";
        public string Number { get; set; } = DateTime.Now.ToString("ddMMyyyy");

    }
}
