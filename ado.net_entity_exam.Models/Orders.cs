﻿using ado.net_entity_exam.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_entity_exam.Models
{
    public class Orders : Entity
    {
        public Guid CourierId {get; set;} 
        public Guid UserId { get; set; }
        public bool isOrdered { get; set; } = false;

        public virtual User user { get; set; }
        public virtual Сourier courier { get; set; }
    }
}
