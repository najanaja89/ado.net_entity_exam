﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_entity_exam.Models
{
    public class StatisticOutput
    {
        public string Login { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public DateTime SubscriptionExpire { get; set; }
        public double Cost { get; set; }
    }
}
