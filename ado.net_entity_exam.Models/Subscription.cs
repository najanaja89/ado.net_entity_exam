﻿using ado.net_entity_exam.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_entity_exam.Models
{
    public class Subscription:Entity
    {
        public Guid UserId { get; set; }
        public DateTime SubscriptionDate { get; set; } = DateTime.Now;
        public DateTime SubscriptionExpires { get; set; }
        public double SubscriptionCost { get; set; }
        public bool Status { get; set; }
        //public User user { get; set; }
    }
}
