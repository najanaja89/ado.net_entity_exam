﻿using ado.net_entity_exam.DataContext;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ado.net_entity_exam.Models
{
    public class User : Entity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public bool isAdmin { get; set; } = false;
        public bool isSubscribed { get; set; } = false;

        public virtual Subscription Subscription { get; set; }
         //public DateTime SubscriptionDate { get; set; } = DateTime.Now;
    }
}