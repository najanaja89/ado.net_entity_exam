﻿using ado.net_entity_exam.DataContext;
using ado.net_entity_exam.Models;
using System;
using System.IO;
using CsvHelper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ado.net_entity_exam.Services
{
    public class MagazineServices
    {
        public bool UserExist(string login)
        {
            using (var context = new MagazineContext())
            {
                var isExist = context.Users.Any(user => user.Login == login);
                return isExist;
            }
        }

        public User GetUserByLogin(string login)
        {
            login.ToLower();
            using (var context = new MagazineContext())
            {
                var GetUser = context.Users.Where(user => user.Login == login).ToList();
                return GetUser.FirstOrDefault();
            }
        }

        public Сourier GetCourierByLogin(string login)
        {
            login.ToLower();
            using (var context = new MagazineContext())
            {
                var GetUser = context.Сouriers.Where(courier => courier.Login == login).ToList();
                return GetUser.FirstOrDefault();
            }
        }


        public void MonthStatus()
        {
            using (var context = new MagazineContext())
            {
                var statistic = from user in context.Users
                                join subscription in context.Subscriptions on user.Id equals subscription.UserId
                                select new StatisticOutput { Login = user.Login, SubscriptionDate = subscription.SubscriptionDate, SubscriptionExpire = subscription.SubscriptionExpires, Cost = subscription.SubscriptionCost };
                statistic.ToList();

                using (var writer = new StreamWriter(@"D:\test.csv", true))
                using (var csvWriter = new CsvWriter(writer))
                {
                    csvWriter.Configuration.Delimiter = ";";
                    csvWriter.Configuration.HasHeaderRecord = false;
                    csvWriter.Configuration.AutoMap<StatisticOutput>();

                    //csvWriter.WriteHeader<StatisticOutput>();
                    csvWriter.WriteRecords(statistic);

                    writer.Flush();

                    Console.WriteLine("Statistic saved in csv file");
                }

                foreach (var item in statistic)
                {
                    Console.WriteLine(item.Login);
                    Console.WriteLine(item.SubscriptionDate);
                    Console.WriteLine(item.SubscriptionExpire);
                    Console.WriteLine(item.Cost);

                }
            }
        }
        public void UserSubsctibe(User user)
        {
            using (var context = new MagazineContext())
            {
                var userBase = context.Users.Where(userSearch => userSearch.Login == user.Login).ToList().FirstOrDefault();
                string menu = "";
                Subscription subscription = new Subscription
                {
                    UserId = user.Id,
                    Status = true,
                    SubscriptionCost = 1,
                    SubscriptionExpires = DateTime.Now,
                };

                if (user.isSubscribed == false)
                {
                    Console.WriteLine("Choose subscription date");
                    Console.WriteLine("Press 1 to 12 month subscription by 100 money");
                    Console.WriteLine("Press 2 to 24 month subscription by 200 money");
                    Console.WriteLine("Press 3 to 36 month subscription by 300 money");

                    menu = Console.ReadLine();

                    switch (menu)
                    {
                        case "1":
                            subscription.SubscriptionExpires = subscription.SubscriptionExpires.AddMonths(12);
                            subscription.SubscriptionCost = 100;
                            userBase.isSubscribed = true;
                            context.Subscriptions.Add(subscription);
                            context.SaveChanges();
                            break;
                        case "2":
                            subscription.SubscriptionExpires = subscription.SubscriptionExpires.AddMonths(24);
                            subscription.SubscriptionCost = 200;
                            userBase.isSubscribed = true;
                            context.Subscriptions.Add(subscription);
                            context.SaveChanges();
                            break;
                        case "3":
                            subscription.SubscriptionExpires = subscription.SubscriptionExpires.AddMonths(36);
                            subscription.SubscriptionCost = 300;
                            userBase.isSubscribed = true;
                            context.Subscriptions.Add(subscription);
                            context.SaveChanges();
                            break;

                        default:
                            break;
                    }

                }

                else
                {
                    Console.WriteLine("Press 1 to unsubscribe (without refunds!)");
                    Console.WriteLine("Press 2 to submit order");
                    string unsubscribeMenu = Console.ReadLine();
                    switch (unsubscribeMenu)
                    {
                        case "1":
                            userBase.isSubscribed = false;
                            context.SaveChanges();
                            break;

                        case "2":
                            var order = context.Orders.Where(item => item.UserId == user.Id).ToList().FirstOrDefault();

                            if (order == null)
                            {
                                Console.WriteLine("Order not exist");
                            }
                            else
                            {
                                order.isOrdered = true;
                                context.SaveChanges();
                            }
                            break;
                        default:
                            break;
                    }

                }
            }

        }
    }
}
