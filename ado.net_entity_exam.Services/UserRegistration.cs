﻿using ado.net_entity_exam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ado.net_entity_exam.Services
{
    public class UserRegistration
    {
        
        User user = new User
        {
            Login = "",
            Password = "",
            FirstName="",
            LastName="",
            Address=""
        };

        public void ValidationCheck()
        {

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimum8Chars = new Regex(@".{8,}");

            //int lengthPassword = User.Password.Length;
            //int minimalLengthOfPassword = 8;

            Console.WriteLine("Enter Login name: ");
            user.Login = Console.ReadLine().ToLower();

            while (true)
            {
                Console.WriteLine("Enter First Name: ");
                user.FirstName = Console.ReadLine().ToLower();
                if (!Regex.IsMatch(user.FirstName, @"^[\p{L}]+$"))
                {
                    Console.WriteLine("First Name Contains is unacceptable symbols");
                }
                else
                {
                    break;
                }
            }

            while (true)
            {
                Console.WriteLine("Enter Last Name: ");
                user.FirstName = Console.ReadLine().ToLower();
                if (!Regex.IsMatch(user.FirstName, @"^[\p{L}]+$"))
                {
                    Console.WriteLine("Last Name Contains is unacceptable symbols");
                }
                else
                {
                    break;
                }
            }
            Console.WriteLine("Enter Address: ");
            user.Address = Console.ReadLine().ToLower();
            while (true)
            {
                Console.WriteLine("Enter Password: ");
                user.Password = ReadPassword();
                var isValidated = hasNumber.IsMatch(user.Password) && hasUpperChar.IsMatch(user.Password) && hasMinimum8Chars.IsMatch(user.Password);
                if (isValidated)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Password must contain minimum 8 symbols, one upper case letter, one digit");
                }
            }

            while (true)
            {
                Console.WriteLine("Retype Password: ");
                string doublePassword = ReadPassword();
                if (doublePassword != user.Password)
                {
                    Console.WriteLine("Passwords did not match!");
                }
                else break;
            }

        }

        public static string ReadPassword()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {

                        password = password.Substring(0, password.Length - 1);
                        int pos = Console.CursorLeft;
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        Console.Write(" ");
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }
            Console.WriteLine();
            return password;
        }

        public User ExportUser()
        {
            return user;
        }

    }
}
