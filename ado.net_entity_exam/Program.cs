﻿using ado.net_entity_exam.DataContext;
using ado.net_entity_exam.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_entity_exam
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new MagazineContext())
            { 
                UserRegistration userRegistration = new UserRegistration();
                MagazineServices magazineServices = new MagazineServices();

                //Console.WriteLine(shopService.UserExist("najanaja"));
                while (true)
                {
                    string menu="";
                    Console.WriteLine("Press 1 to Register");
                    Console.WriteLine("Press 2 to Log in");
                    Console.WriteLine("Press 0 to exit");
                    menu = Console.ReadLine();


                    switch (menu)
                    {
                        case "1":
                            userRegistration.ValidationCheck();
                            while (true)
                            {
                                if (!magazineServices.UserExist(userRegistration.ExportUser().Login))
                                {
                                    context.Users.Add(userRegistration.ExportUser());
                                    context.SaveChanges();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("User Exist");
                                    break;
                                }
                            }

                            break;

                       
                        case "2":
                            while (true)
                            {
                                string login = "";
                                Console.WriteLine("Enter login");
                                login = Console.ReadLine().ToLower();
                                if (!magazineServices.UserExist(login))
                                {
                                    Console.WriteLine("Users not exist!");
                                    break;
                                }
                                else
                                {
                                    string password = "";
                                    Console.WriteLine("Enter password");
                                    password = UserRegistration.ReadPassword();
                                    if (context.Users.Any(user => user.Password == password))
                                    {
                                        if (magazineServices.GetUserByLogin(login).isAdmin == true)
                                        {
                                            string adminMenu = "";
                                            Console.WriteLine("Press 1 to view statistic for month");
                                            adminMenu = Console.ReadLine();
                                            switch (adminMenu)
                                            {
                                                case "1":
                                                    magazineServices.MonthStatus();
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        else
                                        magazineServices.UserSubsctibe(magazineServices.GetUserByLogin(login));
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Password Incorrect!");
                                        break;
                                    }
                                }
                            }

                            break;
                        case "0":
                            System.Environment.Exit(0);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
